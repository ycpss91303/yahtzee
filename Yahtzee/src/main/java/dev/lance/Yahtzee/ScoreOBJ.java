package dev.lance.Yahtzee;

public class ScoreOBJ {
	private int scoreID;
	private int score;
	private boolean used;
	
	public ScoreOBJ() {
		this.score = 0;
		this.scoreID = 0;
		this.used = false;
	}
	
	public int getScore(){
		return this.score;
	}

	public void setScore(int score){
		this.score = score;
	}
	
	public int getScoreID(){
		return this.scoreID;
	}

	public void setScoreID(int id){
		this.scoreID = id;
	}
	
	public boolean getUsed(){
		return this.used;
	}

	public void setUsed(){
		this.used = true;
	}
}
