package dev.lance.Yahtzee;

import java.io.*;
import java.net.*;

public class SocketServer {
    private ServerSocket ss;
    private int numPlayers;
    private ServerSideConnections player1;
    private ServerSideConnections player2;
    private ServerSideConnections player3;

    private Player playerRoll1, playerRoll2, playerRoll3;
    
    
//    private Player playerRoll1;
//    private Player playerRoll2;
//    private Player playerRoll3;

    private int turnMade;
    private int maxTurns;

    public SocketServer(){
        System.out.println("-----Game Server-----");
        numPlayers = 0;
        turnMade = 0;
        maxTurns = 39;

        try{
            ss = new ServerSocket(51734);
        } catch (IOException ex){
            System.out.println("IOException from GameServer Constroctor");
        }
    }

    public void acceptConnections(){
        try {
            System.out.println("Waiting for connections...");
            while (numPlayers < 3){
                Socket s = ss.accept();
                numPlayers++;
                System.out.println("Player #" + numPlayers + " has connected.");
                ServerSideConnections ssc = new ServerSideConnections(s, numPlayers);
                if(numPlayers == 1){
                    player1 = ssc;
                }else if(numPlayers == 2){
                    player2 = ssc;
                }else{
                    player3 = ssc;
                }
                Thread t = new Thread(ssc);
                t.start();
            }
            System.out.println("We now have 3 players. No longer accepting connections");
            System.out.println("\n-----Game Start-----");
        } catch (IOException ex) {
            System.out.println("IOException from GameServer acceptConnections()");
        }
    }

    private class ServerSideConnections implements Runnable{
        private Socket socket;
        private DataInputStream dataIn;
        private DataOutputStream dataOut;
        private int playerID;
        private Player player;

        public ServerSideConnections(Socket s, int id){
            socket = s;
            playerID = id;
            player = new Player("Player #" + playerID);
            if (playerID == 1){
                playerRoll1 = player;
            }else if (playerID == 2){
                playerRoll2 = player;
            }else{
                playerRoll3 = player;
            }

            try {
                dataIn = new DataInputStream(socket.getInputStream());
                dataOut = new DataOutputStream(socket.getOutputStream());
            } catch (IOException ex) {
                System.out.println("IOException from run() SSC Constroctor");
            }
        }

        public void run(){
            try {
                dataOut.writeInt(playerID);
                dataOut.writeInt(maxTurns);
                dataOut.flush();

                while(playerRoll1.isFinished == false || playerRoll2.isFinished == false || playerRoll3.isFinished == false)
                {
                    if (playerID == 1){
                        playerRoll1.ones.setScore(dataIn.readInt());
                        playerRoll1.twos.setScore(dataIn.readInt());
                        playerRoll1.thrs.setScore(dataIn.readInt());
                        playerRoll1.fous.setScore(dataIn.readInt());
                        playerRoll1.fivs.setScore(dataIn.readInt());
                        playerRoll1.sixs.setScore(dataIn.readInt());
                        playerRoll1.larS.setScore(dataIn.readInt());
                        playerRoll1.smaS.setScore(dataIn.readInt());
                        playerRoll1.fulH.setScore(dataIn.readInt());
                        playerRoll1.t_oak.setScore(dataIn.readInt());
                        playerRoll1.f_oak.setScore(dataIn.readInt());
                        playerRoll1.chan.setScore(dataIn.readInt());
                        playerRoll1.yaty.setScore(dataIn.readInt());
                        playerRoll1.isFinished = dataIn.readBoolean();
                        System.out.println("Player #1 finished a round.");
                        if(playerRoll1.isFinished){
                            System.out.println("Player #1 has completed the game.");
                        }
                    }else if (playerID == 2){
                        playerRoll2.ones.setScore(dataIn.readInt());
                        playerRoll2.twos.setScore(dataIn.readInt());
                        playerRoll2.thrs.setScore(dataIn.readInt());
                        playerRoll2.fous.setScore(dataIn.readInt());
                        playerRoll2.fivs.setScore(dataIn.readInt());
                        playerRoll2.sixs.setScore(dataIn.readInt());
                        playerRoll2.larS.setScore(dataIn.readInt());
                        playerRoll2.smaS.setScore(dataIn.readInt());
                        playerRoll2.fulH.setScore(dataIn.readInt());
                        playerRoll2.t_oak.setScore(dataIn.readInt());
                        playerRoll2.f_oak.setScore(dataIn.readInt());
                        playerRoll2.chan.setScore(dataIn.readInt());
                        playerRoll2.yaty.setScore(dataIn.readInt());
                        playerRoll2.isFinished = dataIn.readBoolean();
                        System.out.println("Player #2 finished a round.");
                        if(playerRoll2.isFinished){
                            System.out.println("Player #2 has completed the game.");
                        }
                    }else if (playerID == 3){
                        playerRoll3.ones.setScore(dataIn.readInt());
                        playerRoll3.twos.setScore(dataIn.readInt());
                        playerRoll3.thrs.setScore(dataIn.readInt());
                        playerRoll3.fous.setScore(dataIn.readInt());
                        playerRoll3.fivs.setScore(dataIn.readInt());
                        playerRoll3.sixs.setScore(dataIn.readInt());
                        playerRoll3.larS.setScore(dataIn.readInt());
                        playerRoll3.smaS.setScore(dataIn.readInt());
                        playerRoll3.fulH.setScore(dataIn.readInt());
                        playerRoll3.t_oak.setScore(dataIn.readInt());
                        playerRoll3.f_oak.setScore(dataIn.readInt());
                        playerRoll3.chan.setScore(dataIn.readInt());
                        playerRoll3.yaty.setScore(dataIn.readInt());
                        playerRoll3.isFinished = dataIn.readBoolean();
                        System.out.println("Player #3 finished a round.");
                        if(playerRoll3.isFinished){
                            System.out.println("Player #3 has completed the game.");
                        }
                    }

                    sentScoresToClient();
                    // turnMade++;
                    // if(turnMade == maxTurns){
                    //     System.out.println("Max turns has been reached.");
                    //     break;
                    // }

                }


                playerRoll1.calculateSumScore();
                playerRoll2.calculateSumScore();
                playerRoll3.calculateSumScore();
                
                int temp = 0;
                if(playerRoll1.currentScore > playerRoll2.currentScore && playerRoll1.currentScore > playerRoll3.currentScore){
                    temp = 1;
                }
                if(playerRoll2.currentScore > playerRoll1.currentScore && playerRoll2.currentScore > playerRoll3.currentScore){
                    temp = 2;
                }
                if(playerRoll3.currentScore > playerRoll1.currentScore && playerRoll3.currentScore > playerRoll2.currentScore){
                    temp = 3;
                }

                dataOut.writeInt(temp);

                player1.closeConnection();
                player2.closeConnection();
                player3.closeConnection();
            } catch (IOException ex) {
                System.out.println("IOException from run() SSC:" + ex);
            }
        }
        
        public void sentScoresToClient() {
        	try {
        		dataOut.writeInt(playerRoll1.ones.getScore());
            	dataOut.writeInt(playerRoll1.twos.getScore());
            	dataOut.writeInt(playerRoll1.thrs.getScore());
            	dataOut.writeInt(playerRoll1.fous.getScore());
            	dataOut.writeInt(playerRoll1.fivs.getScore());
            	dataOut.writeInt(playerRoll1.sixs.getScore());
            	dataOut.writeInt(playerRoll1.larS.getScore());
            	dataOut.writeInt(playerRoll1.smaS.getScore());
            	dataOut.writeInt(playerRoll1.fulH.getScore());
            	dataOut.writeInt(playerRoll1.t_oak.getScore());
            	dataOut.writeInt(playerRoll1.f_oak.getScore());
            	dataOut.writeInt(playerRoll1.chan.getScore());
            	dataOut.writeInt(playerRoll1.yaty.getScore());
//            	dataOut.flush();
            	
            	dataOut.writeInt(playerRoll2.ones.getScore());
            	dataOut.writeInt(playerRoll2.twos.getScore());
            	dataOut.writeInt(playerRoll2.thrs.getScore());
            	dataOut.writeInt(playerRoll2.fous.getScore());
            	dataOut.writeInt(playerRoll2.fivs.getScore());
            	dataOut.writeInt(playerRoll2.sixs.getScore());
            	dataOut.writeInt(playerRoll2.larS.getScore());
            	dataOut.writeInt(playerRoll2.smaS.getScore());
            	dataOut.writeInt(playerRoll2.fulH.getScore());
            	dataOut.writeInt(playerRoll2.t_oak.getScore());
            	dataOut.writeInt(playerRoll2.f_oak.getScore());
            	dataOut.writeInt(playerRoll2.chan.getScore());
            	dataOut.writeInt(playerRoll2.yaty.getScore());
//            	dataOut.flush();
            	
            	dataOut.writeInt(playerRoll3.ones.getScore());
            	dataOut.writeInt(playerRoll3.twos.getScore());
            	dataOut.writeInt(playerRoll3.thrs.getScore());
            	dataOut.writeInt(playerRoll3.fous.getScore());
            	dataOut.writeInt(playerRoll3.fivs.getScore());
            	dataOut.writeInt(playerRoll3.sixs.getScore());
            	dataOut.writeInt(playerRoll3.larS.getScore());
            	dataOut.writeInt(playerRoll3.smaS.getScore());
            	dataOut.writeInt(playerRoll3.fulH.getScore());
            	dataOut.writeInt(playerRoll3.t_oak.getScore());
            	dataOut.writeInt(playerRoll3.f_oak.getScore());
            	dataOut.writeInt(playerRoll3.chan.getScore());
            	dataOut.writeInt(playerRoll3.yaty.getScore());
//            	dataOut.flush();
        	} catch (IOException ex) {
        		System.out.println("IOException from sentScoresToClient() SSC:" + ex);
        	}
            
        }

        public void updatePlayerScoreBoard(){
            try {
            	
            } catch (Exception e) {
                System.out.println("Error at updatePlayerScoreBoard()");
            }
            
        }

        public void sendButtonNum(int n) {
            try {
                dataOut.writeInt(n);
                dataOut.flush();
            } catch (IOException ex) {
                System.out.println("IOException from sendButtonNum() SSC");
            }
        }

        public void closeConnection(){
            try {
                socket.close();
                System.out.println("Connection closed");
            } catch (IOException ex) {
                System.out.println("IOException from cliseConnection() SSC");
            }
        }
    }




    public static void main(String[] args) {
        SocketServer gs = new SocketServer();
        gs.acceptConnections();
    }
}
