package dev.lance.Yahtzee;

import java.util.*;

public class Player {
	static int count = 0;
	// Basic Properties
	int round = 3;
	private String name = "";
	private int id = 0;
	int currentScore = 0;
	int currentRound = 1;
	Boolean isFinished;
	
	// Game Scores
	ArrayList<ScoreOBJ> scores = new ArrayList<ScoreOBJ>();
	ScoreOBJ ones = new ScoreOBJ(), twos = new ScoreOBJ(), thrs = new ScoreOBJ(), 
	fous = new ScoreOBJ(), fivs = new ScoreOBJ(), sixs = new ScoreOBJ(), 
	bons = new ScoreOBJ(), t_oak = new ScoreOBJ(), f_oak = new ScoreOBJ(), 
	smaS = new ScoreOBJ(), larS = new ScoreOBJ(), fulH = new ScoreOBJ(), 
	yaty = new ScoreOBJ(), chan = new ScoreOBJ();
	
	// Dices
	ArrayList<Dice> dices = new ArrayList<Dice>(); //All dices
	
	//Init
	public Player(String name) {
		this.isFinished = false;

		this.name = name;
		
		this.round = 3;
		
		this.id = ++count;
		
		ones.setScoreID(1);
		twos.setScoreID(2);
		thrs.setScoreID(3);
		fous.setScoreID(4);
		fivs.setScoreID(5);
		sixs.setScoreID(6);
		larS.setScoreID(7);
		smaS.setScoreID(8);
		fulH.setScoreID(9);
		t_oak.setScoreID(10);
		f_oak.setScoreID(11);
		chan.setScoreID(12);
		yaty.setScoreID(13);
		
		scores.add(ones);
		scores.add(twos);
		scores.add(thrs);
		scores.add(fous);
		scores.add(fivs);
		scores.add(sixs);
		scores.add(bons);
		scores.add(t_oak);
		scores.add(f_oak);
		scores.add(smaS);
		scores.add(larS);
		scores.add(fulH);
		scores.add(yaty);
		scores.add(chan);
		
		dices.add(new Dice());
		dices.add(new Dice());
		dices.add(new Dice());
		dices.add(new Dice());
		dices.add(new Dice());
	}
	
	//Get player's name
	public String getPlayerName() {
		return this.name;
	}
	
	//Get player's ID
		public int getPlayerID() {
			return this.id;
		}
	
	//Print player's Dice Values
	public String printDicesValues() {
		String top		= "             ";
		String centre	= "Your Rolled: ";
		String bottom	= "             ";
		for (Dice dice: dices) {
			top		+= "-----   ";
			centre 	+= "| " + Integer.toString(dice.getValue()) + " |   ";
			bottom	+= "-----   ";
		}
		String temp = top + "\n" + centre + "\n" + bottom;
		
		System.out.println(temp);
		
		return temp;
	}
	
	public void askHoldOrScore() {
		this.askHoldOrScore(0);
	}
	
	public int askHoldOrScore(int option) {
		Scanner scanner = new Scanner(System.in);

		int selection = option;

		while (selection != 1 && selection != 2 && selection != 3) {
			System.out.println("What action would you like to perform next?");
			System.out.println("(1) Select dice(s) to re-roll");
			System.out.println("(2) Re-roll all the dices");
			System.out.println("(3) Score this round");
			
			selection = scanner.nextInt();
			if(selection == 1){
				// Select hold and re-roll
				selectHoldDices();
			}else if(selection == 2){
				// Re-roll all
				reRollAll();
			}else if(selection == 3){
				this.round = 0;
				score();
			}else{
				selection = 0;
				System.out.println("Type error, please try again!");
			}
		}
		return selection;
	}
	
	public void selectHoldDices() {
		this.selectHoldDices("");
	}
	
	public void selectHoldDices(String selection){
		String holdTemp = "";
		if(selection == "") {
			Scanner scanner = new Scanner(System.in);
			
			System.out.println("Please enter in the dice position that you want to re-roll. Please seperate each number with a <<SPACE>>");

			holdTemp = scanner.nextLine();
		}else {
			holdTemp = selection;
		}

		String[] holdsTemp = holdTemp.split("\\s+");

		List<Integer> holds = new ArrayList<Integer>();

		for (int i = 0; i < holdsTemp.length; i++) {
			holds.add(Integer.parseInt(holdsTemp[i]) - 1);
		}

		for (Dice dice: dices) {
			if(holds.contains(dice.getID() % 5)){
				dice.roll();
			}
		}

		// printDicesValues();
	}
	
	public void reRollAll(){
		for (Dice dice: dices) {
			dice.roll();
		}
	}
	
	public void score(){
		this.score(0);
	}

	public void score(int option){
		Scanner scanner = new Scanner(System.in);

		int selection = 0;
		
		while (selection < 1 || selection > 13) {
			if(option == 0) {
				System.out.println("What category do you want to score the round against? (Please enter the category number)");
				
				selection = scanner.nextInt();
			}else {
				selection = option;
			}
			

			for (ScoreOBJ obj: scores) {
				if(obj.getScoreID() == selection){
					if(obj.getUsed()){
						selection = 0;
						System.out.println("This category has been used! Please try another category.");
						break;
					}
				}
			}

			int tempSum = 0;

			boolean appear_two = false, appear_three = false, appear_four = false, appear_five = false;
			
			Map<Integer, Boolean> points_bool = new Hashtable(); 
			points_bool.put(1, false);
			points_bool.put(2, false);
			points_bool.put(3, false);
			points_bool.put(4, false);
			points_bool.put(5, false);
			points_bool.put(6, false);

			Map<Integer, Integer> points_times = new Hashtable(); 
			points_times.put(1, 0);
			points_times.put(2, 0);
			points_times.put(3, 0);
			points_times.put(4, 0);
			points_times.put(5, 0);
			points_times.put(6, 0);

			switch (selection) {
				case 1:
					// Ones
					for (Dice dice: dices) {
						if(dice.getValue() == 1){
							tempSum += dice.getValue();
						}
					}
					this.ones.setScore(tempSum);
					this.ones.setUsed();
					break;
				case 2:
					// Twos
					for (Dice dice: dices) {
						if(dice.getValue() == 2){
							tempSum += dice.getValue();
						}
					}
					this.twos.setScore(tempSum);
					this.twos.setUsed();
					break;
				case 3:
					// Threes
					for (Dice dice: dices) {
						if(dice.getValue() == 3){
							tempSum += dice.getValue();
						}
					}
					this.thrs.setScore(tempSum);
					this.thrs.setUsed();
					break;
				case 4:
					// Fours
					for (Dice dice: dices) {
						if(dice.getValue() == 4){
							tempSum += dice.getValue();
						}
					}
					this.fous.setScore(tempSum);
					this.fous.setUsed();
					break;
				case 5:
					// Fives
					for (Dice dice: dices) {
						if(dice.getValue() == 5){
							tempSum += dice.getValue();
						}
					}
					this.fivs.setScore(tempSum);
					this.fivs.setUsed();
					break;
				case 6:
					// Sixes
					for (Dice dice: dices) {
						if(dice.getValue() == 6){
							tempSum += dice.getValue();
						}
					}
					this.sixs.setScore(tempSum);
					this.sixs.setUsed();
					break;
				case 7:
					// Large Straight
					for (Dice dice: dices) {
						points_bool.replace(dice.getValue(), true);
					}
					if(
						(points_bool.get(1) == true && points_bool.get(2) == true && points_bool.get(3) == true && points_bool.get(4) == true && points_bool.get(5) == true) ||
						(points_bool.get(2) == true && points_bool.get(3) == true && points_bool.get(4) == true && points_bool.get(5) == true && points_bool.get(6) == true)
					){
						this.larS.setScore(40);
					}
					this.larS.setUsed();
					break;
				case 8:
					// Small Straight
					for (Dice dice: dices) {
						points_bool.replace(dice.getValue(), true);
					}
					if(
						(points_bool.get(1) == true && points_bool.get(2) == true && points_bool.get(3) == true && points_bool.get(4) == true) ||
						(points_bool.get(2) == true && points_bool.get(3) == true && points_bool.get(4) == true && points_bool.get(5) == true) ||
						(points_bool.get(3) == true && points_bool.get(4) == true && points_bool.get(5) == true && points_bool.get(6) == true)
					){
						this.smaS.setScore(30);
					}
					this.smaS.setUsed();
					break;
				case 9:
					// Full House
					for (Dice dice: dices) {
						points_times.replace(dice.getValue(), (points_times.get(dice.getValue()) + 1));
					}

					for(Map.Entry<Integer, Integer> entry : points_times.entrySet()) {
						// int key = entry.getKey();
						int value = entry.getValue();
						if(value == 2) appear_two = true;
						if(value == 3) appear_three = true;
					}

					if(appear_two == true && appear_three == true) this.fulH.setScore(25);
					this.fulH.setUsed();
					break;
				case 10:
					// Three of a Kind
					for (Dice dice: dices) {
						points_times.replace(dice.getValue(), (points_times.get(dice.getValue()) + 1));
						tempSum += dice.getValue();
					}

					for(Map.Entry<Integer, Integer> entry : points_times.entrySet()) {
						// int key = entry.getKey();
						int value = entry.getValue();
						if(value == 3) appear_three = true;
						if(value == 4) appear_four = true;
						if(value == 5) appear_five = true;
					}

					if(appear_three == true || appear_four == true || appear_five == true) this.t_oak.setScore(tempSum);
					this.t_oak.setUsed();
					break;
				case 11:
					// Four of a Kind
					for (Dice dice: dices) {
						points_times.replace(dice.getValue(), (points_times.get(dice.getValue()) + 1));
						tempSum += dice.getValue();
					}

					for(Map.Entry<Integer, Integer> entry : points_times.entrySet()) {
						// int key = entry.getKey();
						int value = entry.getValue();
						if(value == 4) appear_four = true;
						if(value == 5) appear_five = true;
					}

					if(appear_four == true || appear_five == true) this.f_oak.setScore(tempSum);
					this.f_oak.setUsed();
					break;
				case 12:
					// Chance
					for (Dice dice: dices) {
						tempSum += dice.getValue();
					}
					this.chan.setScore(tempSum);
					this.chan.setUsed();
					break;
				case 13:
					// Yahtzee
					for (Dice dice: dices) {
						points_times.replace(dice.getValue(), (points_times.get(dice.getValue()) + 1));
					}

					for(Map.Entry<Integer, Integer> entry : points_times.entrySet()) {
						// int key = entry.getKey();
						int value = entry.getValue();
						if(value == 5) appear_five = true;
					}

					if(appear_five) tempSum = 50;
					this.yaty.setScore(tempSum);
					this.yaty.setUsed();
					break;
				default:
					selection = 0;
					System.out.println("Type error, please try again!");
					break;
			}
		}
		calculateSumScore();
	}
	
	public void calculateSumScore(){
		int sum = 0;

		sum += ones.getScore();
		sum += twos.getScore();
		sum += thrs.getScore();
		sum += fous.getScore();
		sum += fivs.getScore();
		sum += sixs.getScore();

		if(sum >= 63) sum += 35;

		sum += t_oak.getScore();
		sum += f_oak.getScore();
		sum += fulH.getScore();
		sum += smaS.getScore();
		sum += larS.getScore();
		sum += yaty.getScore();
		sum += chan.getScore();

		this.currentScore = sum;

//		clearScreen();
//		reRollAll();
//		System.out.print(reloadScoreBoard());
	}
	
	public String reloadScoreBoard(){
		// Score Board
		String scoreBoard = String.format(""
		+" -----------------------------------------------------------------------------------------------------------------------\n"
		+"|Name:	%s			|	Current Score:	%d			|	Current Round:	%d       |\n"
		+" -----------------------------------------------------------------------------------------------------------------------\n"
		+"|(1)Ones:	%d |(2)Twos: %d |(3)Threes:	%d |(4)Fours:	%d |(5)Fives:	%d |(6)Sixes:	%d|Bonus:	%d	|\n"
		+" -----------------------------------------------------------------------------------------------------------------------\n"
		+"|(7)Large Straight:  %d |	(8)Small Straight:  %d  |	(9)Full House:	%d  |	(10)Three of a Kind:	%d	|\n"
		+" -----------------------------------------------------------------------------------------------------------------------\n"
		+"|(11)Four of a Kind:	%d			|(12)Chance:	%d		|	(13)Yahtzee:	%d		|\n"
		+" -----------------------------------------------------------------------------------------------------------------------\n", 
		this.name, this.currentScore, this.currentRound,
		this.ones.getScore(), this.twos.getScore(), this.thrs.getScore(), this.fous.getScore(), this.fivs.getScore(), this.sixs.getScore(), this.bons.getScore(),
		this.larS.getScore(), this.smaS.getScore(), this.fulH.getScore(), this.t_oak.getScore(),
		this.f_oak.getScore(), this.chan.getScore(), this.yaty.getScore()
		);
		return scoreBoard;
	}
	
	public static void clearScreen() {
		System.out.print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	}
	
	
	
	
	
	
	//Extra--Game Simulation
	public int gameLogic(int[][] diceCombinations) {
		for (int i = 0; i < diceCombinations.length; i++) {
			dices.get(0).setValue(diceCombinations[i][0]);
			dices.get(1).setValue(diceCombinations[i][1]);
			dices.get(2).setValue(diceCombinations[i][2]);
			dices.get(3).setValue(diceCombinations[i][3]);
			dices.get(4).setValue(diceCombinations[i][4]);
			this.score(i+1);
		}
		this.calculateSumScore();
		return this.currentScore;
	}
}
