package dev.lance.Yahtzee;

import java.util.Random;

public class Dice {
	static Random r = new Random();
	static int count = 0;
	
	private int value = 1;
	private int diceID = 1;
	
	// init
	public Dice() {
		this.value = r.nextInt(6)+1;
		diceID = ++count -1;
	}
	
	// re-roll dice
	public Dice roll(){
		this.value = r.nextInt(6)+1;
		return this;
	}
	
	public void setValue(int v) {
		this.value = v;
	}
	
	// return its value
	public int getValue() {
		return this.value;
	}
	
	// return its id
	public int getID() {
		return this.diceID;
	}
}
