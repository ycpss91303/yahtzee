package dev.lance.Yahtzee;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.net.*;

public class SocketClient extends JFrame{
    private int playerID;
    private Player player;
    private Player p1, p2, p3;
    private int maxTurns;
    private int turnMade;
    private int otherPlayer;
    private boolean typeEnabled;


    private ClientSideConnection csc;

    public SocketClient(){
        
    }

    public void setUpGUI(){
        if(playerID == 1){
            System.out.println("You are player #1. Yout go first.");
            otherPlayer = 2;
            typeEnabled = true;
        }else if (playerID == 2){
            System.out.println("You are player #2. Wait for your turn.");
            otherPlayer = 3;
            typeEnabled = false;
            // Thread t = new Thread(new Runnable(){
            //     public void run() {
            //         updateTerm();
            //     }
            // });
            // t.start();
        }else{
            System.out.println("You are player #3. Wait for your turn.");
            otherPlayer = 1;
            typeEnabled = false;
            // Thread t = new Thread(new Runnable(){
            //     public void run() {
            //         updateTerm();
            //     }
            // });
            // t.start();
        }

    }

    public void connectToServer(){
        csc = new ClientSideConnection();
    }

    public void setUpGames() {
        // for (int i = 0; i < maxTurns; i++) {
        //     player.currentRound = i;
	    //     System.out.println("\n\n\n\n\n\n=====================GAME ROUND " + i+1 + "=====================");
	    //     System.out.println("Press any key to start.");
	    //     try{
	    //         System.in.read();

	    //         player.round = 3;
	    //         do {
	    //             player.round --;
	    //             player.printDicesValues();
	    //             if(player.round > 0){
	    //                 player.askHoldOrScore();
	    //             }else{
	    //                 player.score();
	    //             }
        //         } while (player.round > 0);
        //         player.calculateSumScore();
        //         player.clearScreen();
        //         player.reloadScoreBoard();
	    //     }
	    //     catch(Exception e) {
        //         System.out.println("Error in game play");
        //     }
        // }
        // ActionListener al = new ActionListener(){
        //     public void actionPerformed(ActionEvent ae) {
        //         JButton b = (JButton) ae.getSource();
        //         int bNum = Integer.parseInt(b.getText());

        //         // message.setText("You clicked buttin # " + bNum + ". Now wait for player #" + otherPlayer);
        //         turnMade ++;
        //         System.out.println("Turns made: " + turnMade);

        //         typeEnabled = false;

        //         // myPoints += values[bNum - 1];
        //         // System.out.println("My Points: " + myPoints);
        //         csc.sendButtonNum(bNum);

        //         if(playerID == 2 && turnMade == maxTurns){
        //             checkWinner();
        //         }else{
        //             Thread t = new Thread(new Runnable(){
        //                 public void run() {
        //                     updateTerm();
        //                 }
        //             });
        //             t.start();
        //         }
        //     }
        // };

    }

    // public void updateTerm() {
    //     int n = csc.receiveButtonNum();
    //     //System.out.println("Yoyr enemy has " + enemypoints + " points.");
    //     if(playerID == 1 && turnMade == maxTurns){
    //         checkWinner();
    //     }else{
    //         typeEnabled = true;
    //     }
    // }

    // public void toggleButtons() {
    //     b1.setEnabled(buttonsEnabled);
    //     b2.setEnabled(buttonsEnabled);
    //     b3.setEnabled(buttonsEnabled);
    //     b4.setEnabled(buttonsEnabled);
    // }

    // public void updateTerm() {
    //     int n = csc.receiveButtonNum();
    //     message.setText("Yoyr enemy clicked button #" + n + ". Your Turn");
    //     enemypoints += values[n-1];
    //     //System.out.println("Yoyr enemy has " + enemypoints + " points.");
    //     if(playerID == 1 && turnMade == maxTurns){
    //         checkWinner();
    //     }else{
    //         buttonsEnabled = true;
    //     }
    //     toggleButtons();
    // }

    private void checkWinner(){
        // buttonsEnabled = false;
        // if(myPoints > enemypoints){
        //     message.setText("You WON!\n" + "YOU: " + myPoints + "\n" + "ENEMY:" + enemypoints);
        // }else if(myPoints < enemypoints){
        //     message.setText("You LOST!\n" + "YOU: " + myPoints + "\n" + "ENEMY:" + enemypoints);
        // }else{
        //     message.setText("It's a tie!\n" + "You both got: " + myPoints + " points");
        // }

        csc.closeConnection();
    }

    //Client Connection Inner Class
    private class ClientSideConnection{
        private Socket socket;
        private DataInputStream dataIn;
        private DataOutputStream dataOut;

        public ClientSideConnection(){
            p1 = new Player("Player #1");
            p2 = new Player("Player #2");
            p3 = new Player("Player #3");
            System.out.println("-----Client-----");
            try {
                socket = new Socket("localhost", 51734);
                dataIn = new DataInputStream(socket.getInputStream());
                dataOut = new DataOutputStream(socket.getOutputStream());

                //get playerID and maxTurns
                playerID = dataIn.readInt();
                player = new Player("Player #" + playerID);
                System.out.println("Conected to server as Player #" + playerID + ".");

                maxTurns = dataIn.readInt() / 3;
                // System.out.println("maxTurns:" + maxTurns);
                System.out.println("\n\n" + player.reloadScoreBoard());

                for (int i = 0; i < maxTurns; i++) {
                    player.currentRound = (i+1);
                    System.out.println("\n\n\n\n\n\n=====================GAME ROUND " + (i+1) + "=====================");
                    System.out.println("Press any key to start.");
                    try{
                        System.in.read();
        
                        player.round = 3;
                        do {
                            player.round --;
                            player.printDicesValues();
                            if(player.round > 0){
                                player.askHoldOrScore();
                            }else{
                                player.score();
                            }
                        } while (player.round > 0);
                        player.calculateSumScore();
                        player.clearScreen();
                        // System.out.println(player.reloadScoreBoard());
                        
                        //Sent Data out
                        dataOut.writeInt(player.ones.getScore());
                        dataOut.writeInt(player.twos.getScore());
                        dataOut.writeInt(player.thrs.getScore());
                        dataOut.writeInt(player.fous.getScore());
                        dataOut.writeInt(player.fivs.getScore());
                        dataOut.writeInt(player.sixs.getScore());
                        dataOut.writeInt(player.larS.getScore());
                        dataOut.writeInt(player.smaS.getScore());
                        dataOut.writeInt(player.fulH.getScore());
                        dataOut.writeInt(player.t_oak.getScore());
                        dataOut.writeInt(player.f_oak.getScore());
                        dataOut.writeInt(player.chan.getScore());
                        dataOut.writeInt(player.yaty.getScore());
                        dataOut.flush();
                        if(i == 12){
                            dataOut.writeBoolean(true);
                        }else{
                            dataOut.writeBoolean(false);
                        }
                        dataOut.flush();
                        
                        try {
                        	p1.ones.setScore(dataIn.readInt());
                        	p1.twos.setScore(dataIn.readInt());
                        	p1.thrs.setScore(dataIn.readInt());
                        	p1.fous.setScore(dataIn.readInt());
                        	p1.fivs.setScore(dataIn.readInt());
                        	p1.sixs.setScore(dataIn.readInt());
                        	p1.larS.setScore(dataIn.readInt());
                        	p1.smaS.setScore(dataIn.readInt());
                        	p1.fulH.setScore(dataIn.readInt());
                        	p1.t_oak.setScore(dataIn.readInt());
                        	p1.f_oak.setScore(dataIn.readInt());
                        	p1.chan.setScore(dataIn.readInt());
                        	p1.yaty.setScore(dataIn.readInt());
                        	
                        	p2.ones.setScore(dataIn.readInt());
                        	p2.twos.setScore(dataIn.readInt());
                        	p2.thrs.setScore(dataIn.readInt());
                        	p2.fous.setScore(dataIn.readInt());
                        	p2.fivs.setScore(dataIn.readInt());
                        	p2.sixs.setScore(dataIn.readInt());
                        	p2.larS.setScore(dataIn.readInt());
                        	p2.smaS.setScore(dataIn.readInt());
                        	p2.fulH.setScore(dataIn.readInt());
                        	p2.t_oak.setScore(dataIn.readInt());
                        	p2.f_oak.setScore(dataIn.readInt());
                        	p2.chan.setScore(dataIn.readInt());
                        	p2.yaty.setScore(dataIn.readInt());
                        	
                        	p3.ones.setScore(dataIn.readInt());
                        	p3.twos.setScore(dataIn.readInt());
                        	p3.thrs.setScore(dataIn.readInt());
                        	p3.fous.setScore(dataIn.readInt());
                        	p3.fivs.setScore(dataIn.readInt());
                        	p3.sixs.setScore(dataIn.readInt());
                        	p3.larS.setScore(dataIn.readInt());
                        	p3.smaS.setScore(dataIn.readInt());
                        	p3.fulH.setScore(dataIn.readInt());
                        	p3.t_oak.setScore(dataIn.readInt());
                        	p3.f_oak.setScore(dataIn.readInt());
                        	p3.chan.setScore(dataIn.readInt());
                        	p3.yaty.setScore(dataIn.readInt());
                        	
                        	
                        	p1.calculateSumScore();
                        	p2.calculateSumScore();
                        	p3.calculateSumScore();
                        	
                            System.out.println(p1.reloadScoreBoard());
                            System.out.println(p2.reloadScoreBoard());
                            System.out.println(p3.reloadScoreBoard());
                        } catch (Exception e) {
                            System.out.println("Error at printAllScoreboards():" + e);
                        }
                    }
                    catch(Exception e) {
                        System.out.println("Error in game play");
                    }
                }

                try {
                    System.out.println("-----Game Over-----\nWaiting for result...");
                    System.out.println("The winner is Player #" + dataIn.readInt());
                } catch (Exception e) {
                    System.out.println("Error in announcing winner");
                }



            } catch (IOException ex) {
                System.out.println("IOException from CSC constructor");
            }
        }

        public void printAllScoreboards() {
            
            
        }

        public void sendButtonNum(int n) {
            try {
                dataOut.writeInt(n);
                dataOut.flush();
            } catch (IOException ex) {
                System.out.println("IOException from sendButtonNum() CSC");
            }
        }

        public int receiveButtonNum() {
            int n = -1;
            try {
                n = dataIn.readInt();
                System.out.println("Player #" + otherPlayer + " clicked button #" + n);
            } catch (Exception e) {
                System.out.println("IOException from receiveButtonNum() CSC");
            }
            return n;
        }

        public void closeConnection(){
            try {
                socket.close();
                System.out.println("-----CONNECTION CLOSED-----");
            } catch (IOException ex) {
                System.out.println("IOException from closeConnection() CSC");
            }
        }
    }

    public static void main(String[] args){
        SocketClient sc = new SocketClient();
        sc.connectToServer();
        // sc.setUpGames();
    }
}
