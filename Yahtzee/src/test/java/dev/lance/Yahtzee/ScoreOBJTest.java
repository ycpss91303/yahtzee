package dev.lance.Yahtzee;

import static org.junit.Assert.*;

import org.junit.Test;

public class ScoreOBJTest {
	
	ScoreOBJ score = new ScoreOBJ();

	@Test
	public void testGetScoreValue() {
		assertTrue("Get Score Object Score Test", score.getScore() == 0);
	}

	@Test
	public void testSetScoreValue() {
		score.setScore(10);
		assertTrue("Set Score Object Score Test", score.getScore() == 10);
	}
	
	@Test
	public void testGetScoreID() {
		assertTrue("Get Score Object ID Test", score.getScoreID() == 0);
	}

	@Test
	public void testSetScoreID() {
		score.setScoreID(1);
		assertTrue("Set Score Object ID Test", score.getScoreID() == 1);
	}
	
	@Test
	public void testGetScoreUsed() {
		assertTrue("Get Score Object Used Test", score.getUsed() == false);
	}

	@Test
	public void testSetScoreUsed() {
		score.setUsed();
		assertTrue("Set Score Object Used Test", score.getUsed() == true);
	}

}
