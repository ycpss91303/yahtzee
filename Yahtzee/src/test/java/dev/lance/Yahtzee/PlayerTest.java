package dev.lance.Yahtzee;

import static org.junit.Assert.*;

import org.junit.Test;

public class PlayerTest {

	// Init a new player, and give its name as Sam
	Player player = new Player("Sam");
	
	//Try to get player's ID
	@Test
	public void testGetPlayerID() {
		assertTrue("Get Player's ID",(player.getPlayerID() == 1));
	}
	
	//Try to get player's name.
	@Test
	public void testGetPlayerName() {
		assertTrue("Get Player's Name",(player.getPlayerName() == "Sam"));
	}
	
	//Try to get player's dice numbers.
	@Test
	public void testGetPlayerDiceNumber() {
		assertTrue("Get 1st Dice value",(player.dices.get(0).getValue() >= 1 && player.dices.get(0).getValue() <= 6));
		assertTrue("Get 2nd Dice value",(player.dices.get(1).getValue() >= 1 && player.dices.get(1).getValue() <= 6));
		assertTrue("Get 3rd Dice value",(player.dices.get(2).getValue() >= 1 && player.dices.get(2).getValue() <= 6));
		assertTrue("Get 4th Dice value",(player.dices.get(3).getValue() >= 1 && player.dices.get(3).getValue() <= 6));
		assertTrue("Get 5th Dice value",(player.dices.get(4).getValue() >= 1 && player.dices.get(4).getValue() <= 6));
	}
	
	//Try to print player's dice numbers.
	@Test
	public void testPrintPlayerDiceNumberInString() {
		assertTrue("Print player's dice numbers to String",(player.printDicesValues().isEmpty() == false));
	}
	
	//Try to loadScoreBoard.
	@Test
	public void testPrintScoreBoard() {
		System.out.print(player.reloadScoreBoard());
		assertTrue("Print player's Score Board",(player.reloadScoreBoard().isEmpty() == false));
	}
	
	//Try to make user choice - 1
	@Test
	public void testUserSelection1() {
		assertTrue("User Selection 1",(player.askHoldOrScore(1) == 1));
	}
	
	//Try to make user choice - 2
	@Test
	public void testUserSelection2() {
		assertTrue("User Selection 2",(player.askHoldOrScore(2) == 2));
	}
	
	//Try to make user choice - 3
	@Test
	public void testUserSelection3() {
		assertTrue("User Selection 3",(player.askHoldOrScore(3) == 3));
	}
	
	//Try to select held dices and re-roll them
	@Test
	public void testSelectHoldDices() {
		player.dices.get(0).setValue(7);
		player.dices.get(1).setValue(7);
		player.dices.get(2).setValue(7);
		player.dices.get(3).setValue(7);
		player.dices.get(4).setValue(7);
		
		player.selectHoldDices("1 2 3 4 5");
		
//		System.out.println(player.dices.get(0).getID() + ": " + player.dices.get(0).getValue());
//		System.out.println(player.dices.get(1).getID() + ": " + player.dices.get(1).getValue());
//		System.out.println(player.dices.get(2).getID() + ": " + player.dices.get(2).getValue());
//		System.out.println(player.dices.get(3).getID() + ": " + player.dices.get(3).getValue());
//		System.out.println(player.dices.get(4).getID() + ": " + player.dices.get(4).getValue());
		
		assertTrue("Get 1st Dice value",(player.dices.get(0).getValue() >= 1 && player.dices.get(0).getValue() <= 6));
		assertTrue("Get 2nd Dice value",(player.dices.get(1).getValue() >= 1 && player.dices.get(1).getValue() <= 6));
		assertTrue("Get 3rd Dice value",(player.dices.get(2).getValue() >= 1 && player.dices.get(2).getValue() <= 6));
		assertTrue("Get 4th Dice value",(player.dices.get(3).getValue() >= 1 && player.dices.get(3).getValue() <= 6));
		assertTrue("Get 5th Dice value",(player.dices.get(4).getValue() >= 1 && player.dices.get(4).getValue() <= 6));
	}
	
	//Try to re-roll all
	@Test
	public void testRerollAll() {
		player.dices.get(0).setValue(7);
		player.dices.get(1).setValue(7);
		player.dices.get(2).setValue(7);
		player.dices.get(3).setValue(7);
		player.dices.get(4).setValue(7);
		
		player.reRollAll();
		
		assertTrue("Get 1st Dice value",(player.dices.get(0).getValue() >= 1 && player.dices.get(0).getValue() <= 6));
		assertTrue("Get 2nd Dice value",(player.dices.get(1).getValue() >= 1 && player.dices.get(1).getValue() <= 6));
		assertTrue("Get 3rd Dice value",(player.dices.get(2).getValue() >= 1 && player.dices.get(2).getValue() <= 6));
		assertTrue("Get 4th Dice value",(player.dices.get(3).getValue() >= 1 && player.dices.get(3).getValue() <= 6));
		assertTrue("Get 5th Dice value",(player.dices.get(4).getValue() >= 1 && player.dices.get(4).getValue() <= 6));
	}
	
	//Try to score
	@Test
	public void testScore1() {
		player.dices.get(0).setValue(1);
		player.dices.get(1).setValue(1);
		player.dices.get(2).setValue(1);
		player.dices.get(3).setValue(1);
		player.dices.get(4).setValue(1);
		
		player.score(1);
		
		assertTrue("Get the score of ones",(player.ones.getScore() == 5));
	}
	
	@Test
	public void testScore2() {
		player.dices.get(0).setValue(2);
		player.dices.get(1).setValue(2);
		player.dices.get(2).setValue(2);
		player.dices.get(3).setValue(2);
		player.dices.get(4).setValue(2);
		
		player.score(2);
		
		assertTrue("Get the score of twos",(player.twos.getScore() == 10));
	}
	
	@Test
	public void testScore3() {
		player.dices.get(0).setValue(3);
		player.dices.get(1).setValue(3);
		player.dices.get(2).setValue(3);
		player.dices.get(3).setValue(3);
		player.dices.get(4).setValue(3);
		
		player.score(3);
		
		assertTrue("Get the score of threes",(player.thrs.getScore() == 15));
	}
	
	@Test
	public void testScore4() {
		player.dices.get(0).setValue(4);
		player.dices.get(1).setValue(4);
		player.dices.get(2).setValue(4);
		player.dices.get(3).setValue(4);
		player.dices.get(4).setValue(4);
		
		player.score(4);
		
		assertTrue("Get the score of fours",(player.fous.getScore() == 20));
	}
	
	@Test
	public void testScore5() {
		player.dices.get(0).setValue(5);
		player.dices.get(1).setValue(5);
		player.dices.get(2).setValue(5);
		player.dices.get(3).setValue(5);
		player.dices.get(4).setValue(5);
		
		player.score(5);
		
		assertTrue("Get the score of fives",(player.fivs.getScore() == 25));
	}

	@Test
	public void testScore6() {
		player.dices.get(0).setValue(6);
		player.dices.get(1).setValue(6);
		player.dices.get(2).setValue(6);
		player.dices.get(3).setValue(6);
		player.dices.get(4).setValue(6);
		
		player.score(6);
		
		assertTrue("Get the score of sixes",(player.sixs.getScore() == 30));
	}
	
	@Test
	public void testScore7() {
		player.dices.get(0).setValue(2);
		player.dices.get(1).setValue(3);
		player.dices.get(2).setValue(4);
		player.dices.get(3).setValue(5);
		player.dices.get(4).setValue(6);
		
		player.score(7);
		
		assertTrue("Get the score of Large Straight",(player.larS.getScore() == 40));
	}
	
	@Test
	public void testScore8() {
		player.dices.get(0).setValue(1);
		player.dices.get(1).setValue(3);
		player.dices.get(2).setValue(4);
		player.dices.get(3).setValue(5);
		player.dices.get(4).setValue(6);
		
		player.score(8);
		
		assertTrue("Get the score of Small Straight",(player.smaS.getScore() == 30));
	}
	
	@Test
	public void testScore9() {
		player.dices.get(0).setValue(3);
		player.dices.get(1).setValue(3);
		player.dices.get(2).setValue(3);
		player.dices.get(3).setValue(2);
		player.dices.get(4).setValue(2);
		
		player.score(9);
		
		assertTrue("Get the score of Full House",(player.fulH.getScore() == 25));
	}
	
	@Test
	public void testScore10() {
		player.dices.get(0).setValue(6);
		player.dices.get(1).setValue(6);
		player.dices.get(2).setValue(6);
		player.dices.get(3).setValue(1);
		player.dices.get(4).setValue(1);
		
		player.score(10);
		
		assertTrue("Get the score of Three of a Kind",(player.t_oak.getScore() == 20));
	}
	
	@Test
	public void testScore11() {
		player.dices.get(0).setValue(6);
		player.dices.get(1).setValue(6);
		player.dices.get(2).setValue(6);
		player.dices.get(3).setValue(6);
		player.dices.get(4).setValue(1);
		
		player.score(11);
		
		assertTrue("Get the score of Four of a Kind",(player.f_oak.getScore() == 25));
	}
	
	@Test
	public void testScore12() {
		player.dices.get(0).setValue(1);
		player.dices.get(1).setValue(2);
		player.dices.get(2).setValue(3);
		player.dices.get(3).setValue(4);
		player.dices.get(4).setValue(5);
		
		player.score(12);
		
		assertTrue("Get the score of Chance",(player.chan.getScore() == 15));
	}
	
	@Test
	public void testScore13() {
		player.dices.get(0).setValue(6);
		player.dices.get(1).setValue(6);
		player.dices.get(2).setValue(6);
		player.dices.get(3).setValue(6);
		player.dices.get(4).setValue(6);
		
		player.score(13);
		
		assertTrue("Get the score of YAHTZEE",(player.yaty.getScore() == 50));
	}
	
	//Try to calculate SumScore
	@Test
	public void testCalSumScore1() {
		player.ones.setScore(1);
		player.twos.setScore(2);
		player.thrs.setScore(3);
		player.fous.setScore(4);
		player.fivs.setScore(5);
		player.sixs.setScore(6);
		player.t_oak.setScore(30);
		player.f_oak.setScore(30);
		player.fulH.setScore(25);
		player.smaS.setScore(0);
		player.larS.setScore(0);
		player.yaty.setScore(0);
		player.chan.setScore(0);
		
		player.calculateSumScore();
		
		assertTrue("Get the Sum Score 1",(player.currentScore == 106));
	}
	
	//Try to calculate SumScore
	@Test
	public void testCalSumScore2() {
		player.ones.setScore(5);
		player.twos.setScore(10);
		player.thrs.setScore(15);
		player.fous.setScore(20);
		player.fivs.setScore(25);
		player.sixs.setScore(30);
		player.t_oak.setScore(0);
		player.f_oak.setScore(0);
		player.fulH.setScore(0);
		player.smaS.setScore(30);
		player.larS.setScore(40);
		player.yaty.setScore(50);
		player.chan.setScore(30);
		
		player.calculateSumScore();
		
		assertTrue("Get the Sum Score 2",(player.currentScore == 290));
	}
	
	//Try to run game logic
	@Test
	public void testGameLogic1() {
		
		int[][] dices = {
			{1,1,6,6,6},
			{6,6,6,2,2},
			{4,3,3,1,5},
			{6,6,4,6,4},
			{6,5,6,5,6},
			{6,1,1,1,6},
			{2,3,4,5,6},
			{1,3,4,5,6},
			{3,3,3,5,5},
			{6,6,1,1,1},
			{6,3,3,3,3},
			{1,3,2,3,1},
			{5,5,5,5,5}
		};
		
		assertTrue("Test Game Logic 1",(player.gameLogic(dices) == 230));
	}

	@Test
	public void testGameLogic2() {
		
		int[][] dices = {
			{1,1,1,6,6},
			{6,6,2,2,2},
			{4,3,3,3,5},
			{4,6,4,6,4},
			{5,5,6,5,6},
			{6,6,1,1,6},
			{2,3,4,5,6},
			{1,3,4,5,6},
			{3,3,3,5,5},
			{6,6,1,1,1},
			{6,3,3,3,3},
			{1,3,2,3,1},
			{5,5,5,5,5}
		};
		
		assertTrue("Test Game Logic 2",(player.gameLogic(dices) == 286));
	}
}
