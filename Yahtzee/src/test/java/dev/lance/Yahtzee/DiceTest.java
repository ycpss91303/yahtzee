package dev.lance.Yahtzee;

import static org.junit.Assert.*;

import org.junit.Test;

public class DiceTest {
	// Init a  new dice.
	Dice dice = new Dice();

	//After created a new dice, try to get its value.
	@Test
	public void testRoll() {
		assertTrue("Get Dice Initial Value Test",(dice.getValue() >= 1 && dice.getValue() <= 6));
	}
	
	//Re-roll the dice, and test if its value is still between 1 and 6.
	@Test
	public void testToString() {
		dice.roll();
		assertTrue("Get Dice Re-roll Value Test",(dice.getValue() >= 1 && dice.getValue() <= 6));
	}
	
	//Get Dice's ID
	@Test
	public void getDiceID() {
		int id = dice.getID();
		assertTrue("Get Dice ID Test",(dice.getID() == id));
	}
}
